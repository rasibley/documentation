---
title: Testing for kernel merge requests on GitLab
description: Workflow and features for testing GitLab Merge Requests
linkTitle: Testing GitLab MRs
weight: 20
---

CKI can test kernels living in GitLab as well. Currently this setup is only
deployed for GitLab.com, and as such this guide will reference it directly.
Administrative setup description can be found under the documentation for
[pipeline triggering], these docs focus on the kernel engineer side.

> Throughout this guide, contacting CKI team is mentioned several times. You
> can do so by tagging `@cki-project` group on `gitlab.com`, sending an email
> to `cki-project@redhat.com` or pinging us on `#kernelci` IRC channel. You
> can also contact individual team members of the project, who are listed in
> the [members] tab of the project.

## Trusted and external contributors

Contributors are split into two groups - *trusted* and *external*. In general,
to be considered a trusted contributor you need to be a [project member]. As a
project member, you get access to trigger testing pipelines in [cki-runs] group.
The pipeline will run in the project and branch linked from the `trigger`
section in the `.gitlab-ci.yml`, and the pipeline status will be reflected on
the merge request. Configuration options for the pipeline can be overriden by
changing the `variables` section of the `.gitlab-ci.yml` file. This can be
useful if e.g. your changes modify the target to create `srpm`, like
`kernel-ark` switched from `make rh-srpm` to `make dist-srpm`. Currently,
documentation for (most) of the configuration options is available in the
[pipeline-trigger repository].

If you are an external contributor, the pipelines will fail with permission
issues. Instead, a bot will trigger the pipelines with predefined configuration
options and link them in the comment. The comment with pipeline status will be
updated as the pipelines finish.

While external contributors can submit changes to `gitlab-ci.yml` file, these
changes are **not** reflected in the created pipelines due to security concerns
and only take effect on subsequent pipelines after the changes are merged.
Configuration options used for external contributors' pipelines are stored
outside of kernel sources and any changes to them need to be ACKed by either
kernel maintainers or CKI team.

<!-- markdownlint-disable no-trailing-punctuation -->
## What will a high level CI flow look like?
<!-- markdownlint-restore -->

If you want any kernels built or tested, you have to submit a merge request.
This is used as an explicit "I want CI" request as opposed to testing any pushes
to git branches, to conserve resources when no testing is required. **Merge
requests marked as "WIP:" or "Draft:" are also tested**. These are useful e.g.
if your changes are not yet ready or are a throwaway (e.g. building a kernel
for support). The prefix indicates to reviewers they don't have to spend their
time looking into the merge request.

Once you submit a merge request, a CI pipeline will automatically start. If you
want to, you can follow the pipeline along (e.g. by clicking on the "Detached
merge request pipeline" link on the merge request web page) to check progress.
Full testing will likely take a few hours. If the pipeline fails, you'll
automatically get an email from GitLab. This can be changed in your
[notification settings]. The pipeline can fail for different reasons and most
of them require your attention, so we suggest to check the status periodically
if you decide to turn the emails off.

CKI team has implemented [automatic retries] to handle intermittent
infrastructure failures and outages. Such failed jobs will be automatically
retried using exponential back off and thus **no action is required from you on
infrastructure failures**. In general, **any other failures are something you
should check out**. Follow the information in the job output to find out what
happened and fix up the merge request if needed.

> If you think there's a CI or test bug, have any issues figuring out what's
> going on, or need to add new build dependencies or features please contact
> the CKI team or the appropriate test maintainer.

### Debug kernel testing

By default, CKI does *not* build or test debug kernels. If you wish to trigger
debug kernel testing, you can do so by manually starting a "debug" pipeline
branch. Open the pipeline view on the merge request, find the debug build job,
and click the `play` button. Alternatively you can click on the job itself and
then press `Trigger this manual action` which has the same results.

No other actions are required, the debug kernel testing will happen the same
way as the regular pipeline testing does. Note that debug kernel testing is
currently only enabled for RHEL8 `x86_64`.

## Testing outside of CI pipeline

Not all kernel tests are onboarded into CKI and thus they can't run as part of
the merge request pipeline. Please consider automating and [onboarding] as many
kernel tests as possible to increase the test coverage and avoid having to
manually trigger the tests every time they are needed.

For testing outside of CI pipelines, developers and QE and encouraged to reuse
kernels built by CKI. Kernel tarballs or RPM repository links (usable by `dnf`
and `yum`) -- depending on which are applicable -- are provided in the pipeline
output. If your team has been integrated into CKI via UMB, CKI will continue to
provide you with messages containing these links. Private (RHEL) kernels will
be mirrored internally so no extra setup for authentication is needed to access
the binaries. Note that generic merge request access (e.g. to check the changes
or add a comment with results) *will* require authentication and you may want
to set up an account to provide results back on the merge request.

UMB testing will also support extended test sets. These are meant to substitute
scratch build testing done by QE/CI and will *not* run automatically but an
explicit request will be required. Please talk with your QE/CI counterparts if
you wish to enable such testing and which test sets you want to support so CKI
can provide the information to all developers.

### Targeting specific hardware

CKI automatically picks any supported machine fitting the test requirements.
This setup is good for default sanity testing, but doesn't work if you need to
run generic tests on specific hardware. One such example is running generic
networking tests on machines with specific drivers that are being modified. The
team is working on extending the patch analysis tool to add device and driver
filters based on kernel changes, so that targeted hardware is picked where
applicable inside the CKI pipeline. Note that this mapping needs to be added to
specific release/driver/architecture cases manually as first it needs to be
verified that machines fitting all filters are available for CKI use. **If you
feel a commonly used mapping combination is missing and would be useful to
include automatically please reach out to the CKI team.**

For cases not covered by the mapping mentioned above, CKI will provide a bot to
allow any trusted contributor to trigger extra CKI testing on specific machines.
The bot will leave a comment on every merge request outlining how to trigger the
testing. Beaker driver filter and specific hostname usage will be supported.
This bot will likely handle extended UMB testing mentioned earlier as well.
*Note that this testing is not a substitute for passing merge request pipelines
as generic sanity testing is still required to make sure there are no unexpected
side effects of the changes.*

## Debugging and fixing failures - more details

To check failure details, click on the pipeline jobs marked with `!` or a red
`X`. Those are the ones that failed. You'll see output of the pipeline, with
some notes about what's being done. Links to e.g. full build logs or kernel
repos will be provided towards the end, together with some more human readable
information. Check those out to identify the source of the problem. Fix any
issues that look to be introduced by your changes and push new versions (or even
debugging commits). Testing will be automatically retried every time.

As mentioned previously, no steps are needed for infrastructure failures as
affected jobs will be automatically retriggered. Infrastructure issues include
(but are not limited to) e.g. pipeline jobs failing to start, jobs crashing due
to lack of resources on the node they ran on, or container registry issues. If
in doubt, feel free to retry the job yourself.

### Known failures / issues

Tests may fail due to already existing kernel or test bugs or infrastructure
issues. Unless they are crashing the machines, we'll keep the tests enabled for
two reasons:

- To not miss any new bugs they could detect
- To make QE's job easier with fix verification; if a kernel bug is fixed the
  test will stop failing and it will be clearly visible

After testing finishes, the results and logs are submitted into our testing
database and checked for matches of any already known issues. If a known issue
is detected, the test failure is marked as such. Pipelines containing only known
failures and no other issues will be considered passing.

It's both test maintainers' and developers' / maintainers' responsibility to
submit known issues into the database. If a new issue is added to the database,
all previously submitted untagged failures are automatically rechecked and
marked in the database (not in the existing CI pipelines!) if they match, and
the issue will be correctly recognized on any future CI runs. Please reach out
to the CKI team for permissions to submit new issues.

### CI pipeline needs to be changed

1. **Your changes are adding new build dependencies.** New packages can't be
   installed ad-hoc, they need to be added to the builder container. If you can,
   please submit a merge request with the new dependency to the [container
   image repository].

2. **Your changes require pipeline configuration changes.** This is the case of
   the `make dist-srpm` example from above. Check out the information provided
   in the `.gitlab-ci.yml` file and see if you can adjust the options.

3. **Your changes are incompatible with the current process and the pipeline
   needs to be adjusted to handle it, a new feature is requested, or a bug is
   detected.** This should rarely happen but in case it does, please contact
   the CKI team to work out what's needed.

If you need any help or assistance with any of the steps, feel free to contact
the CKI team!

## Future plans

Once the new workflow is well established, merge requests will start to be
blocked from being integrated on two occasions. Of course exceptions can be
evaluated on a case by case basis and emergencies with explicit approvals.

### Blocking on missing targeted testing

Every change needs to have a test associated with it (or at least one that
exercises the area) to verify it works properly. You can find whether such tests
are already onboarded into CKI via the targeted testing stats in the pipeline
logs.

Please work with your QE counterparts to create and onboard tests so your
changes can be properly tested.

In case no targeted test is detected, integration of the merge request will
require an explicit approval and potentially also manual verification. Targeted
hardware testing and extended UMB testing *may* be used to override the missing
flag, but as this requires a manual step, please still consider including as
many tests as possible directly!

### Blocking on failing CI pipelines

No changes introducing new problems should be integrated into the kernel. All
observed failures need to be related to known issues, if it's not a known
failure it needs to be fixed before the merge request can be included.

[members]: https://gitlab.com/groups/cki-project/-/group_members
[pipeline triggering]: ../hacking/operations/pipeline-triggering.md#kernels-living-in-gitlabcom
[project member]: https://docs.gitlab.com/ee/user/project/members/
[cki-runs]: https://gitlab.com/cki-runs
[pipeline-trigger repository]: https://gitlab.com/cki-project/pipeline-trigger/#full-list-of-supported-configuration-options
[notification settings]: https://docs.gitlab.com/ee/user/profile/notifications.html#issue--epics--merge-request-events
[automatic retries]: https://gitlab.com/cki-project/pipeline-herder
[onboarding]: ../test-maintainers/onboarding.md
[container image repository]: https://gitlab.com/cki-project/containers
