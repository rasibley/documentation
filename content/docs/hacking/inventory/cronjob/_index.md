---
title: "Cron jobs"
description: Various little pieces of code that have to be run regularly
layout: inventory-table
weight: 30
---

The cron jobs are configured by the [scheduler]. They can either run as a
GitLab CI/CD schedule or a Kubernetes CronJob.

[scheduler]: https://gitlab.com/cki-project/cki-tools/-/tree/main/cki/deployment_tools/scheduler
