---
title: Container images
description: |
    Lots of information on how to CKI team creates, builds, tests and deploys
    container images used for services, cron jobs and the pipeline
weight: 20
---

The CKI Project uses container images for most of it services and cron jobs as
well as in the kernel build and test pipeline.

## Repositories

Container images are built in two places:

- the [container image repository]
- various git repositories in the `cki-project` group on [gitlab.com] and
  [gitlab.cee.redhat.com]

The container image repository contains the container image build files (i.e.
Dockerfiles) for

- some basic container images that are used all across the CKI project (`base`,
  `buildah`, `python`)
- the builder images used in the pipeline (`builder-*`)

For the micro services and cron jobs, the container image build files are
distributed alongside the source code.

## General guidelines

- All containers other than the builder images should be directly or indirectly
  based on the latest Fedora release
- Outside of the container image repository, all container images for services
  and cron jobs should derive from the `base` container image built in the
  container image repository
- For nearly all GitLab CI purposes, the `python` container image built in the
  container image repository should be used

## Creating container images

Container image build files (i.e. Dockerfiles) should be placed in
`builds/image-name.in`. Files ending in `.in` will be preprocessed with the C
preprocessor `cpp` by `buildah`, and are highly recommended. This allows to
include the following building blocks with `#include`:

- `setup-base`: `FROM` command for a generic Python application based on the
  `base` container image with support for customizing image name and tag
- `python-requirements`: commands for a generic Python application that
  contains `setup.cfg` or `requirements.txt` for dependencies and a top-level
  `run.sh` as the entry point
- `cleanup`: remove caches from `dnf` and `pip`, should always be at the end of
  your image build file

As an example, an image build file for a Python application in
`builds/image-name.in` could look like

```text
#include "setup-base"
/* any steps here that should happen before pip install */
#include "python-requirements"
/* any steps here that should happen after pip install */
#include "cleanup"
```

As the preprocessor will consider anything starting with a `#` as a
preprocessor command, comments need to be put between C-style delimiters like
`/* comment */`.

## Building container images

A helper script [cki\_build\_image.sh] that simplifies building and
pushing images is provided in the container image repository and included in
the `buildah` image.

The script will use `buildah` with an image build file from `builds/` via the
`IMAGE_NAME` environment variable to build a container image. `IMAGE_NAME`
should contain the base file name of the image build file located under the
`builds/`, without extension, i.e. `IMAGE_NAME=image-name` for
`builds/image-name.in`.

When run inside a GitLab pipeline, the script will try to determine the tag for
the container images automatically, and push to the project container image
registry if possible.

### Building in OpenShift

When the `OPENSHIFT_REGISTRY` environment variable is defined, the
build will happen as a BuildConfig/Build inside the current context accessible
via `oc`. The built image will be pushed to the given registry by using the
push secret specified in `OPENSHIFT_PUSH_SECRET_NAME`.

### Building foreign architectures

When setting the `IMAGE_ARCH` environment variable to a non-empty value, images
can be built for non-native architectures via qemu. The resulting images have
tags with an appended `-arch`. Currently, `amd64`, `arm64`, `ppc64le` and
`s390x` are verified to be compatible with quay.io.

When setting the `IMAGE_ARCHES` environment variable to a stringified list of
architectures, a multi-arch manifest can be built and uploaded. The source
images are downloaded from the registry before creating the manifest to work
around some peculiarities of quay.io.

### Building locally

To build a container image locally, run the helper script in the `buildah`
container with the correct `IMAGE_NAME` environment variable via

```shell
podman run \
    --rm \
    -e IMAGE_NAME=image-name \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

This will mount the current directory as `/code` inside the container, and call
`cki_build_image.sh` with the correct `IMAGE_NAME` environment variable.
Additionally, it will share the container storage of you local user in
`~/.local/share/containers` with the container. In this way, the resulting
container image will be available on the host as well.

### Building via GitLab

The container image repository contains common GitLab CI building blocks for
building container images via the [gitlab-cki.yml] include file.

It can be included in `.gitlab-ci.yml` with something like

```yaml
include:
  - project: 'cki-project/containers'
    file: '/includes/gitlab-cki.yml'
```

It provides the following job templates for container image building:

- `.publish`: publish a container image named after the project (`CKI_PROJECT_NAME`)
- `.publish_job`: publish a container image named after the job (`CKI_JOB_NAME`)

These jobs will build and push container images for all merge requests, tags,
the tip of the default branch and when pipelines are triggered externally. For
Git tags, the tag name will also be used to tag the image. For merge requests,
a tag of the form `mr-1234` will be used. On the default branch, `latest` is
used.

As an example, to build a container image out of an application, add a
container image build file in `builds/project-name.in` and configure a publish
job in `.gitlab-ci.yml` like this:

```yaml
publish:
  extends: .publish
```

#### Customizing parameters

Take a look at the [publish job template] to see what variables can be
overridden.  The `BASE_IMAGE_TAG` and `buildah_image_tag` variables allow to
run image builds with newer versions of the `base` and `buildah` container
images.

#### Building multiple images

If multiple container images should be built, add multiple container image
build files in `builds/` and configure corresponding jobs in `.gitlab-ci.yml`
extending `.publish_job` instead, like

```yaml
backend:
  extends: .publish_job

frontend:
  extends: .publish_job
```

#### Building multi-arch images

To build multi-arch container images, one job per architecture plus one job for
the multi-arch manifest need to be defined:

```yaml
backend:
  extends: .publish_job
  variables:
    IMAGE_NAME: backend
  parallel:
    matrix:
      - IMAGE_ARCH: [amd64, arm64, ppc64le, s390x]

backend-multi:
  extends: .publish_job
  stage: 🎁
  variables:
    IMAGE_ARCHES: "amd64 arm64 ppc64le s390x"
    IMAGE_NAME: backend
```

## Testing container images

For all merge requests in projects that use container images, updated container
images with the code in the merge request will be published to the GitLab
package registry of the project with tags like `mr-1234`, where `1234`
correspondings to the merge request ID.

### Testing locally

To run container images locally, use a temporary container via

```bash
podman run \
    --rm \
    --env ENV_NAME="value" \
    --env ... \
    --workdir /code \
    registry.gitlab.com/cki-project/project/image
```

The `--env` parameters allow to set environment variables inside the container.
For local development, adding `--volume .:/code` parameter will overlay the
current directory on top of `/code` inside the container. This means that any
changes outside the container will be immediately visible inside the container,
and that it is not necessary to rebuild the container image for each code
change; a restart of the container is good enough.

### Testing OpenShift services

The [deployment-all] repository contains information on how to create
[non-production deployments for services].

### Testing OpenShift cron jobs

Clone the cron job configuration into a new pod via `oc debug` while overriding
the container image and `IS_PRODUCTION` environment variable like

```bash
oc debug \
    cronjob/scheduler-lookaside-kernel-fixes \
    --image registry.gitlab.com/cki-project/schedule-bot/schedule-bot:mr-73 \
    IS_PRODUCTION=false
```

### Testing in the pipeline

The [pipeline-definition] repository contains references to each container
image that the pipeline uses.

To test new container images:

- create a new branch
- change the `{image_tag}` and `{builder_image_tag}` variables in
  `cki_pipeline.yml` and `gitlab-ci.yml`
- push your branch to GitLab
- start a merge request
- add a comment to the merge request that contains: `@cki-ci-bot test`
- wait for the pipelines to finish
- verify everything is correct

## Deploying container images

### Deploying OpenShift services

All services are deployed via container images with the `:latest` tag. As the
container images for that tag are automatically built from the default branch
for all repositories, any update to the default branch of a repository results
in the immediate (~15 minutes) deployment of the new code. This is implemented
via [OpenShift ImageStreams] used in all `DeploymentConfigs`. They are setup to
automatically import newer versions of images via the `--scheduled` flag. When
a newer version of an image is available, services are automatically
redeployed.

The precise configuration for production deployments of services can be found
in [deployment-all/openshift].

### Deploying OpenShift cron jobs

Cron jobs run via container images with the `:latest` tag as well. As that tag
is built automatically from the default branch for all repositories, any update
to the default branch of a repository results in the immediate use of the new
code the next time the cron jobs runs. This implemented via the
`imagePullPolicy=Always` setting.

The precise configuration for production deployments of cron jobs can be found
in [deployment-all/schedules].

### Deploying in the pipeline

The pipeline uses container images with date-based tags.

To release a *tested* (see above) container image, create and push a new tag via

```shell
git tag 20191031.0
git push origin 20191031.0
```

It is also possible to create tags in GitLab's interface via *Repository* →
*Tags* → *New tag*.

All tags must be in the `YYYYMMDD.release` format. For example the first
release on October 31, 2019 would be: `20191031.1`. If a new release is needed
on the same day, increment the release number by one to make `20191031.2`.

Once the CI pipelines are finished, the container images should be visible in
GitLab's container repository. Verify that by clicking *Packages* and then
*Container Registry*.

To deploy new container images:

- create a new branch
- change the `{image_tag}` and `{builder_image_tag}` variables in
  `cki_pipeline.yml` and `gitlab-ci.yml`
- push your branch to GitLab
- start a merge request
- add a comment to the merge request that contains: `@cki-ci-bot test`
- wait for the pipelines to finish
- verify everything is correct
- request a review of the merge request

## Container image registries

### registry.gitlab.com

All container images are hosted on `registry.gitlab.com`. As PSI OpenShift
might have transient problems to pull unauthenticated images from there, a
group deploy token with `read_registry` scope is used for all pulls.

### quay.io

All container images are also mirrored on `quay.io` in the `cki` organization.

### Docker Hub

Docker Hub has introduced [pull rate limits] that make automated use of images
hosted there very unreliable. For that reason, container images consumed from
Docker Hub that are not available elsewhere are mirrored into the GitLab
container image registry connected to the [container image repository].

As an example, the latest [alpine image] is used for CI/CD in the
[cki-artifacts-bucket-listing repository]. The image is hosted on Docker Hub
and is normally referenced either via the short image name `alpine:latest` or
the full image name `docker.io/alpine:latest`. To improve reliability, the
image is mirrored in the [mirror-docker-io] job in the [container image
repository] into the GitLab container image registry. To use the mirrored
image, the image name has been replaced by
`registry.gitlab.com/cki-project/containers/mirror/alpine:latest`.

[gitlab.com]: https://gitlab.com/cki-project/
[gitlab.cee.redhat.com]: https://gitlab.cee.redhat.com/cki-project/
[container image repository]: https://gitlab.com/cki-project/containers
[OpenShift ImageStreams]: https://docs.openshift.com/container-platform/4.6/openshift_images/image-streams-manage.html
[pipeline-definition]: https://gitlab.com/cki-project/pipeline-definition
[deployment-all]: https://gitlab.cee.redhat.com/cki-project/deployment-all
[deployment-all/openshift]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/tree/main/openshift
[deployment-all/schedules]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/tree/main/schedules
[non-production deployments for services]: https://gitlab.cee.redhat.com/cki-project/deployment-all#non-production-deployments
[gitlab-cki.yml]: https://gitlab.com/cki-project/containers/-/blob/main/includes/gitlab-cki.yml
[publish job template]: https://gitlab.com/cki-project/containers/-/blob/main/includes/gitlab-cki.yml
[cki\_build\_image.sh]: https://gitlab.com/cki-project/containers/-/blob/main/cki_build_image.sh
[pull rate limits]: https://www.docker.com/increase-rate-limits
[alpine image]: https://hub.docker.com/_/alpine
[cki-artifacts-bucket-listing repository]: https://gitlab.com/cki-project/cki-artifacts-bucket-listing
[mirror-docker-io]: https://gitlab.com/cki-project/containers/-/blob/main/.gitlab-ci.yml
